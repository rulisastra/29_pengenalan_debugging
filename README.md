# 29_pengenalan_debugging
tools dan fitur Debugging | Pak Kiswono

### tugas
Buat laporan PDF (screenshotnya kecil2 saja) untuk mendemokan debugging dengan breakpoint, step, dan watch untuk go/nodejs/apapun.
```
Max 100 <12:30
Max 90 <23:59
Max 80 <08:00
```

### Resources
- Sentry https://github.com/getsentry/sentry-go 
- Crashlytics https://firebase.google.com/docs/crashlytics 
- Delve https://github.com/go-delve/delve 
- https://www.youtube.com/watch?v=VBiFiguj52I 
- https://www.youtube.com/watch?v=r033vEzL6a4 
- http://blog.ralch.com/tutorial/golang-debug-with-delve/ 
- NodeJS debugging https://code.visualstudio.com/docs/nodejs/nodejs-debugging 
